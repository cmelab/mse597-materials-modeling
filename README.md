## Welcome

I've created this repository to help other students with MSE597.
A static version of the project2 example can be viewed
[here](http://nbviewer.jupyter.org/urls/bitbucket.org/cmelab/mse597-materials-modeling/raw/1e449df0d197b7c543ae7c3f6f45d95b3b16d637/project2/example.ipynb)


## Get the source

```
git clone --recursive  git@bitbucket.org:cmelab/mse597-materials-modeling.git
```

This repo uses git submodules. Either clone with the ``--recursive`` option, or execute ``git submodule update --init``
to fetch the submodules.