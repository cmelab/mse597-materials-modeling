import hoomd
import hoomd.md
#these four lines get the basic infrastructure set up
hoomd.context.initialize('--mode=cpu')
hoomd.init.create_lattice(unitcell=hoomd.lattice.sc(a=2.),n=5)
nl = hoomd.md.nlist.cell()
all = hoomd.group.all()

#Investigating interactions is a lot of Project 2
#Therefore, lots of your work will focus on modifications and additions to these two lines
lj = hoomd.md.pair.lj(r_cut=2.5,nlist=nl)
lj.pair_coeff.set('A','A',epsilon=1,sigma=1)

#Another component of Project 2 is investigating which integrators make sense to use,
#how they influence the results, and what 'extra work' you may need to do to use them well
hoomd.md.integrate.mode_standard(dt=0.005)
hoomd.md.integrate.nve(group=all)

hoomd.analyze.log(filename="log",quantities=['potential_energy'],period=100,overwrite=True)
hoomd.run(1e4)

